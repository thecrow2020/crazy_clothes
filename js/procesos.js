function inputs() {
    let form = document.getElementsByClassName("data")
    let input = []
    for (let i=0; i<form.length; i++){   
        for (let j=0;j<form[i].children.length; j++) {
            input.push(form[i].children[j].value)
        }  
    }
    return input
}

function InputClear() {
    let form = document.getElementsByClassName("data")
    let clear = []
    for (let i=0; i<form.length; i++){   
        for (let j=0;j<form[i].children.length; j++) {
            clear.push(form[i].children[j].value= "")
        }  
    }
    return clear
}

function msj() {
    let msj = document.getElementsByClassName("datamsj")
    let gmsjs = []
    for (let i=0;i<msj.length;i++) {
        gmsjs.push(msj[i])
    }
    return gmsjs
}

function validar() {
    let [n, a, m, c] = inputs()
    let [m1, m2, m3, m4] = msj()
    let ExpReg = /^[0-9 ]+[a-z]*$/;
    let SexpReg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.([a-zA-Z]{3,4})+$/;
    if (n == "") {
        m1.innerHTML = "Campo *NOMBRE* vacio"
    }else if (n.length < 5 || n.length > 10 || ExpReg.test(n)) {
        m1.innerHTML = "Nombre no valido"
        return false
    }else if (a == "") {
        m2.innerHTML = "campo *APELLIDO* vacio"
        return false
    }else if (a.length > 10 || ExpReg.test(a)) {
        m2.innerHTML = "Excede caracteres o carater no valido"
        return false
    }else if (m == "") {
        m3.innerHTML = "Campo *E-MAIL* vacio"
        return false
    }else if (!SexpReg.test(m)) {
        m3.innerHTML = "Correo Invalio"
        return false
    }else if (c == "" || !ExpReg.test(c)) {
        m4.innerHTML = "Ingrese un numero celular Valido"
        return false
    }else if (c.length < 10 || c.length >10) {
        m4.innerHTML = "Ingrese un numero valido"
        return false
    }else {
        InputClear()
    }
}

function clearMsj() {
    let [msj1, msj2, msj3, msj4] = msj()
    msj1.innerHTML = (""), msj2.innerHTML =(""), msj3.innerHTML =(""), msj4.innerHTML = ("");
}

function clearAll() {
    validar()
    setTimeout(()=> {
        clearMsj()
    },3000)
}

